import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    task: {},
    userList: []
  }
  
  export const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
      setTask: (state,payload) => {
        state.task = payload.payload;
      },
      setUserList: (state,payload) => {
        state.userList = [...payload.payload];
      }
    },
  })
  
  // Action creators are generated for each case reducer function
  export const { setTask,setUserList } = counterSlice.actions
  
  export default counterSlice.reducer