import axios from "axios"

const URL = "http://139.59.253.37:8080/api/task"

export const taskService = {
    getTask: () => {
        return axios({
            url: URL + "/getTask",
            method: "GET",
        })
    },
    createTask:(data,token) => {
        return axios({
            url: URL + "/createTask",
            method: "POST",
            data,
            headers: {
                token
            }
        })
    },
    updateTask:(data,token) => {
        return axios({
            url: URL + "/updateTask",
            method: "POST",
            data,
            headers: {
                token
            }
        })
    }
}