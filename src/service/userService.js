import axios from "axios"

const URL = "http://139.59.253.37:8080/api/user"
export const userService = {
    getLogin: (username,password) => {
        return axios({
            url: URL + "/login",
            method: "POST",
            data : {
                username,
                password
            }
        })
    },
    getUser: () => {
        return axios({
            url: URL + "/userList",
            method: "GET"
        })
    }
}