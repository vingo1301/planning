import React, { useState } from 'react'
import Amin_Task from '../../Component/Admin_Task/Admin_Task';
import { sessionService } from '../../service/sessionService';
import { useNavigate } from 'react-router';

export default function HomePage() {
  const navigate = useNavigate();
  
    let user = sessionService.getUser();
    
      if (user == null){
        // document.location.href = "/login";
        setTimeout(() => {
          navigate("/login");
        }, 0);
      }
    
    
  return (
    <div id='homepage' className='bg-gray-200 px-3'>
        
        <Amin_Task></Amin_Task>
    </div>
  )
}
