import React from 'react'
import "./LoginPage.css"
import { Input } from 'antd';
import { EyeTwoTone,EyeInvisibleOutlined } from '@ant-design/icons';
import logo from "../../isset/img/logo-header.png"
import { userService } from '../../service/userService';
import { sessionService } from '../../service/sessionService';
import { ToastContainer, toast } from 'react-toastify';
import { useNavigate } from 'react-router';


export default function LoginPage() {
    const navigate = useNavigate();
    const handleLogin = () => {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        if(username.length == 0){
            document.getElementById("warning").innerHTML = "Vui lòng nhập Tài Khoản";
            return;
        }
        if(password.length == 0){
            document.getElementById("warning").innerHTML = "Vui lòng nhập Mật Khẩu";
            return;
        }
        document.getElementById("warning").innerHTML = "";
        userService.getLogin(username,password).then((res) => {
                sessionService.setUser(res.data);
                console.log("hello");
                toast.success('Đăng nhập thành công!',{
                    position: toast.POSITION.TOP_CENTER
                  });
                setTimeout(() =>{
                    navigate("/");
                },1000)
              })
              .catch((err) => {
               console.log(err);
               toast.error('Tài khoản hoặc mật khẩu không chính xác!',{
                position: toast.POSITION.TOP_CENTER
              });
              });
    }
    
  return (
    <div id='login' className='relative' onKeyDown={function(e){if(e.key=="Enter")return handleLogin()}}>
        <ToastContainer></ToastContainer>
        <div className='login-form rounded-xl shadow-xl w-1/3 h-4/6 bg-white absolute opacity-95'>
            <div className='mx-5'>
                <img className='mx-auto mt-6' width={"100px"} src={logo} alt="" />
                <h2 className='text-3xl font-bold font-mono text-sky-500 my-2'>LOGIN</h2>
                <div className='mb-8 form-input flex'>
                    <i class="fa fa-user self-center mx-3 text-xl text-sky-500"></i>
                    <Input id='username' placeholder="Tài Khoản" />
                </div>
                <div className='my-10 form-input flex'>
                    <i class="fa fa-lock self-center mx-3 text-xl text-sky-500"></i>
                    <Input.Password id='password' placeholder="Mật Khẩu"
                    iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                    />
                </div>
                <div><span className='text-red-500 ' id='warning'></span></div>
                <button onClick={handleLogin} type="button" class="text-white bg-gradient-to-br from-sky-800 to-sky-300 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800 font-medium rounded-2xl px-5 py-2.5 text-center mt-2 mr-2 mb-2 text-xl">Đăng Nhập</button>
            </div>
        </div>
    </div>
  )
}
