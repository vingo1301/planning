import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import { UploadOutlined } from '@ant-design/icons';
import { message, Upload } from 'antd';
import moment from 'moment/moment';

export default function Conversation() {
    const task = useSelector((state) => state.counter.task);
    const fileList = [];
    const props = {
      name: 'file',
      action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
      headers: {
        authorization: 'authorization-text',
      },
      onChange(info) {
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };
    // fake data bình luận
    const [comment,setComment] = useState([
      {
        comment_id:1,
        user_id: 1,
        user_name: "Vĩ",
        binh_luan: "Làm lẹ lẹ cái tay lên!",
        time: "2023-05-17 16:00"
      },
      {
        comment_id:2,
        user_id: 2,
        user_name: "Phát",
        binh_luan: "Cố quá quá cố!",
        time: "2023-05-17 17:00"
      }
    ]);
    //
    let newComment = {
        comment_id: "",
        user_id: 1,
        user_name: "Vĩ",
        binh_luan: "",
        time: "2023-05-17 17:00"
    }
    const renderComment = () => {
      return comment.map((comment) => {
        return <div className='comment text-left px-3 py-2 my-3'>
          <h3 className='text-lg text-blue-500 font-semibold mb-3'>{comment.user_name}<span className='mx-3 text-red-500'>{moment(comment.time).format("HH:mm DD/MM/YYYY")}</span></h3>
          <p>{comment.binh_luan}</p>
        </div>
      })
    }
    const addNewComment = () => {
      if(document.getElementById("comment").value == ""){
        toast.error('Vui lòng nhập bình luận!',{
          position: toast.POSITION.TOP_CENTER
        });return;
      }
      newComment.binh_luan = document.getElementById("comment").value;
      let cloneComment = [...comment];
      cloneComment.push(newComment);
      setComment(cloneComment);
    }
    const vietBinhLuan = () => {
      return(
        <>     
                <div id='text-area' class="hidden w-full mb-4 border border-gray-200 rounded-lg bg-gray-50 dark:bg-gray-700 dark:border-gray-600">
                    <div class="px-4 py-2 bg-white rounded-t-lg dark:bg-gray-800">
                        <label for="comment" class="sr-only">Your comment</label>
                        <textarea id="comment" rows="4" class=" w-full px-0 text-sm text-gray-900 bg-white border-0 dark:bg-gray-800 focus:ring-0 dark:text-white dark:placeholder-gray-400" placeholder="Write a comment..." required></textarea>
                    </div>
                    <div class="flex items-center justify-between px-3 py-2 border-t dark:border-gray-600">
                        <button onClick={addNewComment} class="inline-flex items-center py-2.5 px-4 text-xs font-medium text-center text-white bg-blue-700 rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-blue-800">
                            Đăng Bình Luận
                        </button>
                        <div class="flex pl-0 space-x-1 sm:pl-2">
                            <Upload {...props}>
                              <button type="button" class="inline-flex justify-center p-2 text-gray-500 rounded cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600">
                                <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd"></path></svg>
                                <span class="sr-only">Attach file</span>
                              </button>
                            </Upload>
                            <Upload
                              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                              listType="picture"
                              defaultFileList={[...fileList]}
                            >
                              <button type="button" class="inline-flex justify-center p-2 text-gray-500 rounded cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600">
                                <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clip-rule="evenodd"></path></svg>
                                <span class="sr-only">Upload image</span>
                              </button>
                            </Upload>
                        </div>
                    </div>
                </div>
        </>
      )
    }
    const handleShowTextbox = () => {
      document.getElementById("text-area").classList.toggle("hidden");
    }
    const handleClose = () => {
        document.getElementById("conversation").classList.toggle("w-0");
        document.getElementById("conversation").classList.toggle("w-1/2");
    }
  return (
    <div id='conversation' className='overflow-scroll h-full w-0 bg-white absolute top-0 right-0 shadow-sm rounded-md'>
        <div className='text-left'><button onClick={handleClose} className='text-3xl ml-5 mt-3'><i class="fa fa-times"></i></button></div>
        <div className='flex'>
          <p className='self-center text-left ml-3 font-semibold text-xl'>{task.task_name}</p>
          <button onClick={handleShowTextbox} className='btn px-2 py-1 ml-3 bg-slate-50'>Viết Bình Luận...</button>
        </div>
          {vietBinhLuan()}
          {renderComment()}
    </div>
  )
}
