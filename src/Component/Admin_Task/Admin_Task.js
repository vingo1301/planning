import React, {useEffect, useState } from 'react';
import { Select, Space } from 'antd';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { DatePicker} from 'antd';
import { ToastContainer, toast } from 'react-toastify';
import Conversation from "./Conversation";
import { useSelector, useDispatch } from 'react-redux'
import 'react-toastify/dist/ReactToastify.css';
import './AdminTask.css';
import { setTask, setUserList } from '../../reduxToolkit/reducer/counterSlice';
import { taskService } from '../../service/taskService';
import { userService } from '../../service/userService';
import {sessionService} from '../../service/sessionService';
import moment from 'moment/moment';
import { Tooltip } from 'antd';

dayjs.extend(customParseFormat);

const dateFormat = 'YYYY-MM-DD';

export default function Amin_Task() {
    const dispatch = useDispatch();
    useEffect(() => {
                taskService.getTask().then((res) => {
                setListTask(res.data.data);
                setCloneListTask(res.data.data)
              })
              .catch((err) => {
               console.log(err);
              });
              userService.getUser().then((res) => {
                      dispatch(setUserList(res.data.data));
                    })
                    .catch((err) => {
                     console.log(err);
                    });
         },[])
    //  tạo biến state để select người nhận việc
    let [valueList,setValueList] = useState([]);
    // 
    const [dateStart,setDateStart] = useState("2000-01-01");
    const [dateStop,setDateStop] = useState("2099-12-31");
    const [valueTask,setValueTask] = useState([3]);

    let userList = useSelector((state) => state.counter.userList);
    let user = sessionService.getUser();

    const [listTask,setListTask] = useState([]);
    const [cloneListTask,setCloneListTask] = useState([]);
    let newTask = {
            task_name: "",
            task_user:"",
            task_start: "",
            task_finish: "",
            task_done: 0,
            task_note: "",
            user_created: user?.data.user_id,
            task_parent_id:1,
            task_status:1
    }
    // Đóng mở hộp Trao Đổi
    const showConversation = (task_id) => {
        document.getElementById("conversation").classList.toggle("w-1/2");
        document.getElementById("conversation").classList.toggle("w-0");
        let index = listTask.findIndex(function(task){
            return task.task_id == task_id;
        })
        dispatch(setTask(listTask[index]));
    }
    // render các option cho người nhận việc
    const renderValue = () => {
        let array = [];
        userList?.map((user) => {
            array.push({
                value: user.user_id,
                label: user.user_fullname
            })
        });
        setValueList(array);
    }
    // render tên người nhận việc 
    const renderUserFullName = (user_id) => {
        let user = userList?.find((user) => {
            return user.user_id == user_id;
        })
        return user?.user_fullname;
    }
    const renderTask = () => {
        return listTask?.map(task =>{
            return(
                <tr>
                    <td><input key={`task${task.task_id}`} id="default-checkbox" type="checkbox" value="" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/></td>
                    <td><Tooltip title="Nhấn Enter để lưu thay đổi">
                        <input key={`taskName${task.task_id}`} onKeyDown={function(e){if(e.key=="Enter") return hanldeUpdateTask(e,task.task_id)}} className='w-full focus-visible:outline-blue-500' type="text" name='taskName' defaultValue={task?.task_name}/>
                    </Tooltip></td>
                    <td><button key={`taskChat${task.task_id}`} onClick={()=>showConversation(task?.task_id)} className='mx-auto text-gray-600 w-1/3'><svg  viewBox="0 0 20 20" fill="currentColor" width="24" height="24" aria-hidden="true" class="icon_component chat-without-update icon_component--no-focus-style"><path d="M10.4339 1.94996C11.5976 1.94797 12.7458 2.21616 13.7882 2.7334C14.8309 3.25083 15.7393 4.00335 16.4416 4.93167C17.144 5.85999 17.6211 6.93874 17.8355 8.08291C18.0498 9.22707 17.9956 10.4054 17.6769 11.525C17.3583 12.6446 16.7839 13.6749 15.9992 14.5347C15.2145 15.3945 14.2408 16.0604 13.1549 16.4797C12.069 16.8991 10.9005 17.0605 9.7416 16.9513C8.72154 16.8552 7.7334 16.5518 6.83723 16.0612L4.29494 17.2723C3.23222 17.7785 2.12271 16.6692 2.62876 15.6064L3.83948 13.0636C3.26488 12.0144 2.94833 10.8411 2.91898 9.64114C2.88622 8.30169 3.21251 6.97789 3.86399 5.8071C4.51547 4.63631 5.4684 3.66119 6.62389 2.98294C7.77902 2.30491 9.09451 1.94825 10.4339 1.94996ZM10.4339 1.94996C10.4343 1.94996 10.4348 1.94996 10.4352 1.94996L10.4341 2.69996L10.4327 1.94996C10.4331 1.94996 10.4335 1.94996 10.4339 1.94996ZM13.1214 4.07707C12.2868 3.66289 11.3673 3.44821 10.4355 3.44996L10.433 3.44996C9.36086 3.44842 8.30784 3.73382 7.38321 4.27655C6.45858 4.81929 5.69605 5.59958 5.17473 6.53645C4.65341 7.47332 4.39232 8.53263 4.41853 9.60446C4.44475 10.6763 4.75732 11.7216 5.32382 12.6318C5.45888 12.8489 5.47411 13.1197 5.36422 13.3505L4.28601 15.615L6.55002 14.5365C6.78078 14.4266 7.05164 14.4418 7.26869 14.5768C8.05992 15.0689 8.95463 15.3706 9.88231 15.458C10.81 15.5454 11.7453 15.4161 12.6145 15.0805C13.4838 14.7448 14.2631 14.2118 14.8913 13.5236C15.5194 12.8353 15.9791 12.0106 16.2342 11.1144C16.4893 10.2182 16.5327 9.27499 16.3611 8.35913C16.1895 7.44328 15.8076 6.57978 15.2454 5.8367C14.6832 5.09362 13.9561 4.49125 13.1214 4.07707Z" fill="currentColor" fill-rule="evenodd" clip-rule="evenodd"></path><path d="M11.25 6.5C11.25 6.08579 10.9142 5.75 10.5 5.75C10.0858 5.75 9.75 6.08579 9.75 6.5V8.75H7.5C7.08579 8.75 6.75 9.08579 6.75 9.5C6.75 9.91421 7.08579 10.25 7.5 10.25H9.75V12.5C9.75 12.9142 10.0858 13.25 10.5 13.25C10.9142 13.25 11.25 12.9142 11.25 12.5V10.25H13.5C13.9142 10.25 14.25 9.91421 14.25 9.5C14.25 9.08579 13.9142 8.75 13.5 8.75H11.25V6.5Z" fill="currentColor" fill-rule="evenodd" clip-rule="evenodd"></path></svg></button></td>
                    {/* chỗ này lỗi select không lấy được danh sách user ngay từ đầu nên buộc phải in tên user bằng thẻ p. khi click sẽ ẩn thẻ p rồi load danh sách user */}
                    <td className='relative'><p className='absolute bg-white top-0 left-0 z-10 w-full h-full' id={`p${task.task_id}`} onClick={function(e) {
                        renderValue();
                        document.getElementById(`p${task.task_id}`).classList.add("hidden");
                    }}>{renderUserFullName(task.task_user)}</p><Select key={`taskUser${task.task_id}`}
                                    showSearch
                                    defaultValue={task.task_user}
                                    optionFilterProp="children"
                                    onChange={(e) => updateTaskUser(e,task.task_id)}
                                    style={{width:"100%"}}
                                    // onSearch={}
                                        filterOption={(input, option) =>
                                        (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                                    }
                                     options={valueList}
                            /></td>
                    <td id ="task_done_select">
                        <Select
                            key={`taskDone${task.task_id}`}
                            defaultValue={task.task_done}
                            style={{width:"100%"}}
                            onChange={(e) => updateTaskDone(e,task.task_id)}
                            options={[
                            {
                            value: 1,
                            label: <p className='bg-green-500 text-white w-full'>Đã Hoàn Thành</p>,
                            },
                            {
                            value: 0,
                            label: <p className='bg-yellow-500 text-white'>Đang Thực Hiện</p>,
                            },
                            {
                            value: 2,
                            label: <p className='bg-red-500 text-white'>Đã Huỷ</p>,
                            },
                            ]}
                            />
                    </td>
                    <td><DatePicker key={`taskStart${task.task_id}`} style={{width:"100%"}} defaultValue={dayjs(task?.task_start,dateFormat)} onChange={(date,dateString) => updateTaskStart(date,dateString,task.task_id)} format={dateFormat} /></td>
                    <td><DatePicker style={{width:"100%"}} defaultValue={dayjs(task?.task_finish,dateFormat)} onChange={(date,dateString) => updateTaskFinish(date,dateString,task.task_id)} format={dateFormat} /></td>
                    <td><Tooltip key={`taskFinish${task.task_id}`} title="Nhấn Enter để lưu thay đổi">
                        <input key={`taskNote${task.task_id}`} onKeyDown={function(e){if(e.key=="Enter") return hanldeUpdateTask(e,task.task_id)}} className='w-full focus-visible:outline-blue-500' type="text" name='taskNote' defaultValue={task?.task_note}/>
                    </Tooltip></td>
                </tr>
            )
        })
    }
    const handleAddTask =  () => {
        document.getElementById("new_task")?.classList.remove("hidden");
        document.getElementById("new_taskname")?.focus();
        renderValue();
    }
    // Thay đổi Tên Công Việc của task mới
    const changeTaskName = (e) => {
        newTask.task_name = e.target.value;
    }
    // Thay đổi Người Nhận Việc của task mới
    const changeTaskUser = (value) => {
        newTask.task_user = value;
    }
    // Thay đổi Ngày Bắt Đầu của task mới
    const changeTaskStart = (date,dateString) => {
        newTask.task_start = dateString;
    }
    // Thay đổi Ngày Kết thúc của task mới
    const changeTaskEnd = (date,dateString) => {
        newTask.task_finish = dateString;
    }
    // Thay đổi Ghi Chú của task mới
    const changeTaskNote = (e) => {
        newTask.task_note = e.target.value;
    }
    // Tiến hành Thêm việc mới
    const addNewTask = () => {
        // Validate cho newTask
        if(newTask.task_name == "" || newTask.task_name.length <5){
            toast.error('Tên Công Việc ít nhất 5 ký tự!',{
                position: toast.POSITION.TOP_CENTER
            });return;
        }
        if(newTask.task_user == "" || newTask.task_user == null){
            toast.error('Vui lòng chọn Người thực hiện!',{
                position: toast.POSITION.TOP_CENTER
            });return;
        }
        if(newTask.task_start == "" || newTask.task_start == null){
            toast.error('Vui lòng chọn Ngày bắt đầu!',{
                position: toast.POSITION.TOP_CENTER
            });return;
        }
        if(newTask.task_finish == "" || newTask.task_finish == null){
            toast.error('Vui lòng chọn Ngày kết thúc!',{
                position: toast.POSITION.TOP_CENTER
            });return;
        }
        if(newTask.task_start > newTask.task_finish){
            toast.error('Ngày kết thúc phải lớn hơn ngày bắt đầu!',{
                position: toast.POSITION.TOP_CENTER
            });return;
        }
        // 
        // Đoạn này sẽ tạo API đưa vào database
        taskService.createTask(newTask,user.token).then((res) => {
                // reload lại trang để láy task_id và refresh input
                window.location.reload();
              })
              .catch((err) => {
               console.log(err);
              });
    }
    
    // Thay đổi giá trị lọc của Ngày Bắt Đầu
    const changeDateStart = (date,dateString) => {
        setDateStart(dateString);
      };
      // Thay đổi giá trị lọc của Ngày Kết thúc
    const changeDateStop = (date,dateString) => {
        setDateStop(dateString);
      };
    const handleFilter = (value) => {
        setValueTask(value);
        document.getElementById("cancel-filter").classList.remove("hidden");
        if(dateStart <= dateStop){
            let array = [];
            if(value == 3){
                cloneListTask.map((task) =>{
                    if(task.task_start >= dateStart && task.task_start <= dateStop ){
                        array.push(task);
                    }
                });
            }else{
                cloneListTask.map((task) =>{
                    if(task.task_start >= dateStart && task.task_start <= dateStop && task.task_done == value ){
                        array.push(task);
                    }
                });
            }
            setListTask(array);
        }else{
            toast.error('Ngày kết thúc phải lớn hơn ngày bắt đầu!',{
                position: toast.POSITION.TOP_CENTER});
        }
    }
    const hanldeCancelFilter = () => {
        setListTask(cloneListTask);
    }
    
    
    // update công việc
    let updateApi =  (data) => {
        taskService.updateTask(data,user.token).then((res) => {
            let index = listTask.findIndex((task) => task.task_id == res.data.task_id);
            let clone = [...listTask];
            clone[index] = res.data;
            setListTask(clone);
            let index2 = cloneListTask.findIndex((task) => task.task_id == res.data.task_id);
            let clone2 = [...cloneListTask];
            clone2[index2] = res.data;
            setCloneListTask(clone2);
            document.activeElement.blur();
            toast.success('Cập nhật thành công!',{
                position: toast.POSITION.TOP_CENTER});
          })
          .catch((err) => {
           console.log(err);
          });
    }
    let hanldeUpdateTask = (e,id) => {
        if(e.target.name == "taskName"){
            if(e.target.value.length < 5){
                toast.error('Tên Công Việc ít nhất 5 ký tự!',{
                    position: toast.POSITION.TOP_CENTER
                });return;
            }
            let data = {
                task_id: id,
                task_name:e.target.value
            }
            updateApi(data);return;
        }
        if(e.target.name == "taskNote"){
            let data = {
                task_id: id,
                task_note:e.target.value
            }
            updateApi(data);
        }
    }
    let updateTaskUser = (value,id) => {
        let data = {
            task_id: id,
            task_user: value
        }
        updateApi(data)
    }
    let updateTaskDone = (value,id) => {
        let data = {
            task_id: id,
            task_done: value
        }
        updateApi(data)
    }
    let updateTaskStart = (date,dateString,id) => {
        let data = {
            task_id: id,
            task_start: dateString
        }
        updateApi(data)
    }
    let updateTaskFinish = (date,dateString,id) => {
        let data = {
            task_id: id,
            task_start: dateString
        }
        updateApi(data)
    }
  return (
    <div id='admin_task' className='px-3 bg-white rounded-xl py-10 relative'>
        <div className='button-list grid grid-cols-6'>
            <button onClick={handleAddTask} type="button" class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 my-2"><i class="fa fa-plus mr-3"></i>Tạo công việc mới</button>
            <div className='flex'>
                <h3 className='self-center'>Trạng Thái: </h3>
                <Space wrap>
                    <Select
                        defaultValue={3}
                        style={{
                        width: 120,
                        marginLeft: "5px",
                        }}
                        onChange={handleFilter}
                        options={[
                        {
                            value: 3,
                            label: "Tất Cả",
                        },
                        {
                        value: 1,
                        label: "Hoàn Thành",
                        },
                        {
                        value: 0,
                        label: "Đang Thực Hiện",
                        },
                        {
                        value: 2,
                        label: "Đã Huỷ",
                        },
                        ]}
                        />
                </Space>
            </div>
            <div className='col-span-4 flex'>
                <div className='flex ml-3'>
                    <h3 className='self-center'>Từ: </h3>
                    <div className='p-3'>
                            <DatePicker format={dateFormat} onChange={changeDateStart} />
                    </div>
                    
                </div>
                <div className='flex'>
                    <h3 className='self-center'>Đến: </h3>
                    <div className='p-3'>
                            <DatePicker format={dateFormat} onChange={changeDateStop} />
                    </div>
                </div>
                <button onClick={() => handleFilter(valueTask)} className='bg-blue-500 my-3 text-white px-5 py-1 text-lg font-bold rounded-xl hover:text-blue-500 hover:bg-gray-200'><i class="fa fa-filter"></i> Lọc</button>
                <div className='self-center ml-3 text-xl'><button onClick={hanldeCancelFilter} id='cancel-filter' className='hidden shadow-md py-1 px-2 rounded-md'><i><i class="fa fa-times"></i><span className='text-red-500 ml-2'>Huỷ bộ lọc</span></i></button></div>
                </div>
        </div>
        <div className='my-5'>
                <table className='w-full text-md'>
                    <th>
                        
                    </th>
                    <th>Công Việc</th>
                    <th>Trao Đổi</th>
                    <th>Người Nhận Việc</th>
                    <th>Tiến Trình</th>
                    <th>Ngày Bắt Đầu</th>
                    <th>Ngày Kết Thúc</th>
                    <th>Ghi Chú</th>
                        {renderTask()}
                    {<tr className='hidden' id='new_task'>
                        <td><button onClick={function(){document.getElementById("new_task").classList.add("hidden")}}><i class="fa fa-times text-red-500"></i></button></td>
                        <td><input className='w-full' onChange={changeTaskName} type="text" name='task_name' id='new_taskname' placeholder='Công Việc' /></td>
                        <td></td>
                        <td><Select
                                    showSearch
                                    placeholder="Người Nhận Việc"
                                    optionFilterProp="children"
                                    onChange={changeTaskUser}
                                    style={{width:"100%"}}
                                    // onSearch={}
                                        filterOption={(input, option) =>
                                        (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                                    }
                                     options={valueList}
                            />
                        </td>
                        <td className='bg-yellow-500 text-white'>Đang thực hiện</td>
                        <td><DatePicker onChange={changeTaskStart} format={dateFormat} /></td>
                        <td><DatePicker onChange={changeTaskEnd} format={dateFormat} /></td>
                        <td><input onChange={changeTaskNote} className='w-5/6' type="text" name='task_name' id='new_task' placeholder='Ghi Chú' /><button onClick={addNewTask} className='bg-blue-500 text-white px-3 py-1 rounded-xl font-semibold w-1/6'>Lưu</button></td>
                    </tr>}
                </table>
                <Conversation></Conversation>
                <ToastContainer />
            </div>
    </div>
  )
}
